﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace INIS.Controllers
{
    [Authorize]
    public class INISAdminController : Controller
    {
        [AllowAnonymous]
        public IActionResult Index()
        {
            var isAuthenticated = User.Identity.IsAuthenticated;
            if(!isAuthenticated)
            {
                return RedirectToAction("Wellcome");
            }
            return View();
        }

        public IActionResult Wellcome()
        {
            return RedirectToAction("Index");
        }
    }
}