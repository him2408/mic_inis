#pragma checksum "D:\ASP.NET\Work_git\mic_inis\INIS\INIS\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3e8332fb6eb94d1945542289a186920b568feed5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\ASP.NET\Work_git\mic_inis\INIS\INIS\Views\_ViewImports.cshtml"
using INIS;

#line default
#line hidden
#line 2 "D:\ASP.NET\Work_git\mic_inis\INIS\INIS\Views\_ViewImports.cshtml"
using INIS.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3e8332fb6eb94d1945542289a186920b568feed5", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"142ad51b63ce58f11bab545853468f1c6baef8e3", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "D:\ASP.NET\Work_git\mic_inis\INIS\INIS\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Index";
    Layout = "~/Views/Shared/_LayoutUser.cshtml";

#line default
#line hidden
            BeginContext(94, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            DefineSection("css", async() => {
                BeginContext(110, 11, true);
                WriteLiteral("\r\n    <link");
                EndContext();
                BeginWriteAttribute("href", " href=\"", 121, "\"", 159, 1);
#line 8 "D:\ASP.NET\Work_git\mic_inis\INIS\INIS\Views\Home\Index.cshtml"
WriteAttributeValue("", 128, Url.Content("~/css/index.css"), 128, 31, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(160, 22, true);
                WriteLiteral(" rel=\"stylesheet\" />\r\n");
                EndContext();
            }
            );
            BeginContext(185, 12531, true);
            WriteLiteral(@"
<section class=""banner"" style=""clear: left;"">
    <div class="" banner-main owl-carousel owl-theme"">
        <div class=""banner-item swing-in-top-fwd fill"" style=""background-image: url('image/banner-home.jpg');"">
        </div>
        <div class=""banner-item swing-in-top-fwd fill"" style=""background-image: url('image/banner-home.jpg');"">
        </div>
    </div>
</section>
<section class=""h-project"">
    <div class=""container"">
        <div class=""h-title"">
            <p class=""h-title__main hide"">PROJECTS</p>
            <p class=""h-title__sub""><i class=""fas fa-chevron-right""></i><a href=""project.html"">Projects of the institute</a></p>
        </div>
        <div class=""h-project__list owl-carousel owl-theme wow fadeInUp"">
            <div class=""h-project__item"">
                <a href=""project-detail.html"" class=""h-pro__imgct"">
                    <div class=""h-pro__img"" style=""background-image: url('image/h1.jpg');""></div>
                </a>
                <a data-fancybox data-s");
            WriteLiteral(@"mall-btn=""true"" data-width=""720"" data-height=""440"" href=""image/h1.jpg"" class=""quickview""><i class=""fas fa-arrows-alt""></i></a>
                <div class=""h-pro__infor"">
                    <p class=""h-date"">March 8, 2019</p>
                    <a href=""project-detail.html"" class=""h-ct"">Pellentesque finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non quam tempus, vel fermentum.</a>
                </div>
            </div>
            <div class=""h-project__item"">
                <a href=""project-detail.html"" class=""h-pro__imgct"">
                    <div class=""h-pro__img"" style=""background-image: url('image/h2.jpg');""></div>
                </a>
                <a data-fancybox data-small-btn=""true"" data-width=""720"" data-height=""440"" href=""image/h2.jpg"" class=""quickview""><i class=""fas fa-arrows-alt""></i></a>
                <div class=""h-pro__infor"">
                    <p class=""h-date"">March 8, 2019</p>
     ");
            WriteLiteral(@"               <a href=""project-detail.html"" class=""h-ct"">Pellentesque finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non quam tempus, vel fermentum.</a>
                </div>
            </div>
            <div class=""h-project__item"">
                <a href=""detail.html"" class=""h-pro__imgct"">
                    <div class=""h-pro__img"" style=""background-image: url('image/h3.jpg');""></div>
                </a>
                <a data-fancybox data-small-btn=""true"" data-width=""720"" data-height=""440"" href=""image/h3.jpg"" class=""quickview""><i class=""fas fa-arrows-alt""></i></a>
                <div class=""h-pro__infor"">
                    <p class=""h-date"">March 8, 2019</p>
                    <a href=""detail.html"" class=""h-ct"">Pellentesque finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non quam tempus, vel fermentum.</a>
          ");
            WriteLiteral(@"      </div>
            </div>
            <div class=""h-project__item"">
                <a href=""detail.html"" class=""h-pro__imgct"">
                    <div class=""h-pro__img"" style=""background-image: url('image/h4.jpg');""></div>
                </a>
                <a data-fancybox data-small-btn=""true"" data-width=""720"" data-height=""440"" href=""image/h4.jpg"" class=""quickview""><i class=""fas fa-arrows-alt""></i></a>
                <div class=""h-pro__infor"">
                    <p class=""h-date"">March 8, 2019</p>
                    <a href=""detail.html"" class=""h-ct"">Pellentesque finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non quam tempus, vel fermentum.</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=""h-news"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-lg-6 wow fadeInLeft"">
                <div class=""h-title"">
    ");
            WriteLiteral(@"                <p class=""h-title__main hide"">NEWS & EVENT</p>
                    <p class=""h-title__sub""><i class=""fas fa-chevron-right""></i><a href=""news.html"">Watch the latest news</a></p>
                </div>
                <div class=""h-news-lg"">
                    <div class=""h-project__item"">
                        <a href=""news-detail.html"" class=""h-pro__imgct"">
                            <div class=""h-pro__img"" style=""background-image: url('image/h5.jpg');""></div>
                        </a>
                        <a data-fancybox data-small-btn=""true"" data-width=""720"" data-height=""440"" href=""image/h5.jpg"" class=""quickview""><i class=""fas fa-arrows-alt""></i></a>
                        <div class=""h-pro__infor"">
                            <p class=""h-date"">March 8, 2019</p>
                            <a href=""news-detail.html"" class=""h-ct"">Pellentesque finibus dolor non quam tempus, vel fermentum. Pell finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non q");
            WriteLiteral(@"uam tempus, vel fermentum.</a>
                            <p class=""h-des"">Pellentesque finibus dolor non quam tempus, vel fermentum lacus dictum. Phasellus ultricies aliquam elit ut posuere. In mattis convallis luctus. Donec luctus dapibus facilisis. Pellentesque venenatis mi sed odio lacinia convallis.</p>
                        </div>
                    </div>
                </div>
                <div class=""h-news-small"">
                    <div class=""row"">
                        <div class=""col-6"">
                            <div class=""h-project__item"">
                                <a href=""news-detail.html"" class=""h-pro__imgct"">
                                    <div class=""h-pro__img"" style=""background-image: url('image/h6.jpg');""></div>
                                </a>
                                <a data-fancybox data-small-btn=""true"" data-width=""720"" data-height=""440"" href=""image/h6.jpg"" class=""quickview""><i class=""fas fa-arrows-alt""></i></a>
                      ");
            WriteLiteral(@"          <div class=""h-pro__infor"">
                                    <p class=""h-date"">March 8, 2019</p>
                                    <a href=""news-detail.html"" class=""h-ct"">Pellentesque finibus dolor non quam tempus, vel fermentum. Pell finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non quam tempus, vel fermentum.</a>
                                </div>
                            </div>
                        </div>
                        <div class=""col-6"">
                            <div class=""h-project__item"">
                                <a href=""news-detail.html"" class=""h-pro__imgct"">
                                    <div class=""h-pro__img"" style=""background-image: url('image/h7.jpg');""></div>
                                </a>
                                <a data-fancybox data-small-btn=""true"" data-width=""720"" data-height=""440"" href=""image/h7.jpg"" class=""quickview""><i class=""fas fa-arrows-alt""></i></a>
                                <");
            WriteLiteral(@"div class=""h-pro__infor"">
                                    <p class=""h-date"">March 8, 2019</p>
                                    <a href=""news-detail.html"" class=""h-ct"">Pellentesque finibus dolor non quam tempus, vel fermentum. Pell finibus dolor non quam tempus, vel fermentum. Pellentesque finibus dolor non quam tempus, vel fermentum.</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=""col-lg-6 wow fadeInRight"">
                <div class=""h-news-public"">
                    <a href=""publications.html"" class=""header-main__title"">Publications</a>
                    <div class=""h-public__box"">
                        <div class=""h-box__header fill"" style=""background-image: url('image/box-header.jpg');"">
                            <h3>INDUSTRIAL NETWORK</h3>
                        </div>
                        <div class=""h-box__body"">
  ");
            WriteLiteral(@"                          <div class=""h-body__item"">
                                <div class=""row"">
                                    <div class=""col-2"">
                                        <h3>1</h3>
                                    </div>
                                    <div class=""col-10"">
                                        <a href=""research-detail.html"" class=""h-box__title"">Vivamus feugiat, urna quis finibus euismod, elit erat interdum ex, pretium commodo est libero ut lectus. Vivamus feugiat, urna quis finibus euismod, elit erat interdum ex, pretium commodo est libero ut lectus.</a>
                                        <p class=""h-box__date"">March 31, 2019</p>
                                    </div>
                                </div>
                            </div>
                            <div class=""h-body__item"">
                                <div class=""row"">
                                    <div class=""col-2"">
                                  ");
            WriteLiteral(@"      <h3>2</h3>
                                    </div>
                                    <div class=""col-10"">
                                        <a href=""research-detail.html"" class=""h-box__title"">Vivamus feugiat, urna quis finibus euismod, elit erat interdum ex, pretium commodo est libero ut lectus.</a>
                                        <p class=""h-box__date"">March 31, 2019</p>
                                    </div>
                                </div>
                            </div>
                            <div class=""h-body__item"">
                                <div class=""row"">
                                    <div class=""col-2"">
                                        <h3>3</h3>
                                    </div>
                                    <div class=""col-10"">
                                        <a href=""research-detail.html"" class=""h-box__title"">Vivamus feugiat, urna quis finibus euismod, elit erat interdum ex, pretium commodo est li");
            WriteLiteral(@"bero ut lectus.</a>
                                        <p class=""h-box__date"">March 31, 2019</p>
                                    </div>
                                </div>
                            </div>
                            <div class=""h-body__item"">
                                <div class=""row"">
                                    <div class=""col-2"">
                                        <h3>4</h3>
                                    </div>
                                    <div class=""col-10"">
                                        <a href=""research-detail.html"" class=""h-box__title"">Vivamus feugiat, urna quis finibus euismod, elit erat interdum ex, pretium commodo est libero ut lectus.</a>
                                        <p class=""h-box__date"">March 31, 2019</p>
                                    </div>
                                </div>
                            </div>
                            <div class=""h-body__item"">
                     ");
            WriteLiteral(@"           <div class=""row"">
                                    <div class=""col-2"">
                                        <h3>5</h3>
                                    </div>
                                    <div class=""col-10"">
                                        <a href=""research-detail.html"" class=""h-box__title"">Vivamus feugiat, urna quis finibus euismod, elit erat interdum ex, pretium commodo est libero ut lectus.</a>
                                        <p class=""h-box__date"">March 31, 2019</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=""readmore-research"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-lg-8"">
                <h3 class=""text-center wow fadeInLeft"">Research direction of the institute</h3>
        ");
            WriteLiteral("    </div>\r\n            <div class=\"col-lg-4\">\r\n                <div class=\"text-center\"><a href=\"\" class=\"btn btn-sub wow bounceIn\" style=\"animation-delay: 0.5s;\">READ MORE</a></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
